import nose
import arrow
import acp_times

def test_200():
    #test control km 0 ~ 200
    
    assert acp_times.open_time(100, 200, '2018-11-06T00:00:00') == (arrow.get("2018-11-06T02:56:00").isoformat())
    assert acp_times.close_time(100, 200, '2018-11-06T00:00:00') == (arrow.get('2018-11-06T06:40:00').isoformat())

def test_400():
    #test control km 200 ~ 400
    
    assert acp_times.open_time(300, 400, '2018-11-06T00:00:00') == (arrow.get("2018-11-06T09:00:00").isoformat())
    assert acp_times.close_time(300, 400, '2018-11-06T00:00:00') == (arrow.get('2018-11-06T20:00:00').isoformat())

def test_600():
    #test control km 400 ~ 600
    
    assert acp_times.open_time(500, 600, '2018-11-06T00:00:00') == (arrow.get("2018-11-06T15:28:00").isoformat())
    assert acp_times.close_time(500, 600, '2018-11-06T00:00:00') == (arrow.get('2018-11-07T09:20:00').isoformat())

def test_1000():
    #test control km 600 ~ 1000
    
    assert acp_times.open_time(800, 1000, '2018-11-06T00:00:00') == (arrow.get("2018-11-07T01:57:00").isoformat())
    assert acp_times.close_time(800, 1000, '2018-11-06T00:00:00') == (arrow.get('2018-11-08T09:30:00').isoformat())

def test_over():
    # test control km over brevet, but not over 20% km of brevet_time
    
    assert acp_times.open_time(410, 400, '2018-11-06T00:00:00') == (arrow.get("2018-11-06T12:08:00").isoformat())
    assert acp_times.close_time(410, 400, '2018-11-06T00:00:00') == (arrow.get('2018-11-07T03:00:00').isoformat())
    
    
    assert acp_times.open_time(1100, 1000, '2018-11-06T00:00:00') == (arrow.get("2018-11-07T09:05:00").isoformat())
    assert acp_times.close_time(1100, 1000, '2018-11-06T00:00:00') == (arrow.get('2018-11-09T03:00:00').isoformat())

